const express = require("express");
const mongoose = require("mongoose");


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
      console.log("connected!")
    });

mongoose.connect('mongodb://localhost:27017/rsvp', {
  useNewUrlParser: true,
  useUnifiedTopology:true
}); //Flag for using new Server Discovery and Monitoring engine instead of current (deprecated) one


//storage for those attending/not attending

const responseSchema = new mongoose.Schema({ //import Schema w/o const at top
  name:String, // String is shorthand for {type: String}
  email:String,
  attending:String,
  headCount:String
});

const Response = mongoose.model('Response', responseSchema);

const app = express()

app.use(express.urlencoded({extended:false}))// extended false? Gabby said: 
app.set("view engine", "pug") //sets engine to pug
app.set("views", "./views")// location of "views"

app.get("/",(req, res)=>{
res.render("index")
});

app.post("/reply", async(req, res)=>{
  try{
  //console.log("request",req.body)
  const guest = new Response(req.body);//create new reservation
  console.log("before save", guest);
  await guest.save()
  res.render("reply")
  }
    catch(err){
      console.error(err)
      res.status(500).send(err.message)
    };
    //console.log('saved item');
    //console.log('after save');  
})

app.get("/guests", async(req,res)=>{ // no space between async and params
  let attendingArray = []
  let notAttendingArray = []
await Response.find({}, function (err, response){
  console.log(response)
  response.forEach((person)=>{
    if(person.attending === "I'll be there!"){
      attendingArray.push(person)
    }else {
      notAttendingArray.push(person)
    }
  });
})
  res.render("guests",{
    attendingArray:attendingArray,
    notAttendingArray:notAttendingArray
  })
  console.log(attendingArray)
});


const port = 3000;
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
  });