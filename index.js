const express = require("express");
const mongoose = require("mongoose");


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("we're connected!")
});

mongoose.connect("mongodb://localhost:27017/rsvp", { useNewUrlParser: true, useUnifiedTopology: true });//from Jake

const responseSchema = new mongoose.Schema({ //stack overflow
    name: String,
    email: String,
    attending: String,
    headCount: String
});

const Response = mongoose.model('Response', responseSchema);

const app = express();

app.use(express.urlencoded({ extended: false }))

app.set("view engine", "pug")

app.set("views", "./views")

app.get('/', (req, res) => {
    res.render("index")
})

app.post('/reply', async(req, res) => {
    try{
        const response = new Response(req.body);
        await response.save()
        res.render("reply")
    }
    catch(err){
        console.error(err)
        res.status(500).send(err.message)
    }
})

app.get('/guests', async(req, res) => {
    let weGoin = []
    let weNotGoin = []

    await Response.find({}, function (err, response) {
        console.log(response)
        response.forEach((guest) => {
            if (guest.attending === "I'll be there!") {
                weGoin.push(guest)
            } else {
                weNotGoin.push(guest)
            }
        });
    })
    res.render("guests", {
        weGoin: weGoin,
        weNotGoin: weNotGoin
    })
    console.log(weGoin)
})


const port = 3000

app.listen(port, () => console.log(`Listening at http://localhost:${port}`))
